# README #

This small web page visualizes retrospective data from a JSON file with the help of [D3.js](http://d3js.org/), [NVD3.js](http://nvd3.org/), [AngularJS](https://angularjs.org/) and [Twitter Bootstrap](http://getbootstrap.com/). It uses [gulp](http://gulpjs.com/) for building and serving the web page, and uses [npm](https://www.npmjs.org/) and [Bower](http://bower.io/) for dependency management.

### How do I get set up? ###

* Make sure node and npm are installed
    * Run `node --version` and `npm --version` to verify 
* Clone repo
* cd to repo and run `npm install`
* Run `gulp` to serve web page


### Entering retrospective data ###
* The web page uses two files for storing data: `leadtimes.json` and `retrospectives.json`, clear these files and enter your own data.

### Contribution guidelines ###

* Tests are appreciated but not mandatory