describe('retrospective-service.js', function() {
	var scope,
		service;

	beforeEach(module('app.retrospective'));

	xdescribe('drillDown', function() {

		beforeEach(inject(function($rootScope, _retrospectiveService_) {
			scope = $rootScope;
			service = _retrospectiveService_;
		}));

		// check to see if it has the expected function
		it('should have an drillDown function', function() {
			expect(angular.isFunction(service.drillDown)).toBe(true);
		});

		// check to see if it has the expected function
		it('drillDown should return expected value', function() {

			var obj = {
					person: {
						age: 22
					}
				},
				key = ['person', 'age'];
			expect(service.drillDown(obj, key)).toBe(22);
		});

	});

	xdescribe('getChartValues', function() {

		beforeEach(inject(function($rootScope, _retrospectiveService_) {
			scope = $rootScope;
			service = _retrospectiveService_;
		}));

		// check to see if it has the expected function
		it('should have an getChartValues function', function() {
			expect(angular.isFunction(service.getChartValues)).toBe(true);
		});

		// check to see if it has the expected function
		it('should return correct chart values', function() {

			var data = [{
				date: '2014-10-02',
				barometer: {
					morale: 6.3,
					quality: 6.8,
					flow: 4.2
				}
			}, {
				date: '2014-09-11',
				barometer: {
					morale: 7.5,
					quality: 7.3,
					flow: 6.5
				}

			}];

			expect(service.getChartValues(data, 'barometer.morale')).toEqual([ [1, 6.3], [0, 7.5]]);
		});

	});

	describe('computeBarometerAverages', function() {

		beforeEach(inject(function($rootScope, _retrospectiveService_) {
			scope = $rootScope;
			service = _retrospectiveService_;
		}));

		// check to see if it has the expected function
		it('should have an computeBarometerAverages function', function() {
			expect(angular.isFunction(service.computeBarometerAverages)).toBe(true);
		});

		// check to see if it has the expected function
		it('should return correct average', function() {

			// align
			var data = [{
				date: '2014-10-02',
				barometer: {
					morale: 6.3,
					quality: 6.8,
					flow: 4.2
				}
			}, {
				date: '2014-09-11',
				barometer: {
					morale: [7.5, 7.5],
					quality: 7.3,
					flow: 6.5
				}

			}, {
				date: '2014-09-12',
				barometer: {
					morale: [1,2,3],
					quality: 7.3,
					flow: 6.5
				}

			}];

			// act
			service.computeBarometerAverages(data);

			expect(data[0].barometer.morale).toEqual(6.3);
			expect(data[1].barometer.morale).toEqual(7.5);
			expect(data[2].barometer.morale).toEqual(2);
		});

	});
});