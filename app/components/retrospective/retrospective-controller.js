angular.module('app.retrospective')

.controller('RetrospectiveCtrl', ['$scope', '$http', 'retrospectiveService',
    function($scope, $http, retrospectiveService) {

        $http.get('../../data/retrospectives.json')
            .then(function(res) {
                var retrospectives = res.data;

                retrospectiveService.computeBarometerAverages(retrospectives);

                retrospectiveService.compareRetrospective(retrospectives);

                $scope.retrospectives = retrospectives;

                $scope.moraleChart = retrospectiveService.getChartObjectFor(retrospectives, 'barometer.morale');
                $scope.qualityChart = retrospectiveService.getChartObjectFor(retrospectives, 'barometer.quality');
                $scope.flowChart = retrospectiveService.getChartObjectFor(retrospectives, 'barometer.flow');
            });
    }
]);