'use strict';

angular.module('app.retrospective', ['ui.router'])

.config(['$stateProvider', '$locationProvider',
    function($stateProvider, $locationProvider) {
        var modulePath = '/components/retrospective/';

        $stateProvider
            .state('retrospective', {
                url: '/retrospective',
                templateUrl: modulePath + 'retrospective.html',
                controller: 'RetrospectiveCtrl'
            });

        $locationProvider.html5Mode(true);
    }
]);