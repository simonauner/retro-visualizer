'use strict';

angular.module('app.retrospective')

.factory('retrospectiveService', [

  function($) {
    var service = {};

    var compare = function(a, b) {
      if (a > b) {
        return 1;
      } else if (a < b) {
        return -1;
      }
      return 0;

    };

    var drillDown = function(obj, keyArr) {

      if (keyArr.length === 1) {
        return obj[keyArr[0]];
      } else {
        return drillDown(obj[keyArr[0]], keyArr.splice(1, keyArr.length));
      }

    };


    var getChartValues = function(data, key) {
      var keyArr = key.split('.');

      var arr = data.map(function(currentValue, index, array) {
        return [array.length - (index + 1), drillDown(currentValue, key.split('.'))];
      });
      return arr;
    };

    service.computeBarometerAverages = function(retrospectives) {
      angular.forEach(retrospectives, function(retrospective, index) {
        angular.forEach(retrospective.barometer, function(value, key) {
          if (angular.isArray(retrospective.barometer[key])) {
            var arrayLength = retrospective.barometer[key].length;
            var sum = retrospective.barometer[key].reduce(function(a, b) {
              return a + b;
            });
            retrospective.barometer[key] = (sum / arrayLength);
          }
        });
      });

      return retrospectives;
    };

    service.compareRetrospective = function(retrospectives) {
      angular.forEach(retrospectives, function(retrospective, index) {
        if (!angular.isDefined(retrospectives[index + 1])) {
          return;

        }
        var previous = retrospectives[index + 1];
        retrospective.barometer.compare = {
          morale: compare(retrospective.barometer.morale, previous.barometer.morale),
          quality: compare(retrospective.barometer.quality, previous.barometer.quality),
          flow: compare(retrospective.barometer.flow, previous.barometer.flow)
        };
      });

      return retrospectives;
    };

    service.getChartObjectFor = function(data, key) {
      return [{
        key: key,
        values: getChartValues(data, key)
      }];
    };

    return service;
  }
]);