/* global d3 */
angular.module('app.leadTime')

.controller('LeadTimeCtrl', ['$scope', '$http', 'leadTimeService',
    function($scope, $http, leadTimeService) {

        $http.get('../../data/leadtimes.json')
            .then(function(res) {
                var workItems = res.data;

                $scope.formatFunctionOnlyIntegers = function() {
                    return function(d) {
                        return d3.format('d')(d);
                    };
                };

                $scope.deploysPerWeek = leadTimeService.getDeploysPerWeek(workItems);

                $scope.averageLeadTime = leadTimeService.getAverageLeadTime(workItems);
            });


    }
]);