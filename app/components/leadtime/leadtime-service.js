'use strict';

angular.module('app.leadTime')

.factory('leadTimeService', ['$filter',

	function($filter) {
		var service = {};

		service.getDeploysPerWeek = function(workItems) {
			var weeks = {};
			var weeksArray = [];

			var getWeekNumber = function(dateString) {
				// thanks http://stackoverflow.com/questions/6117814/get-week-of-year-in-javascript-like-in-php

				// Copy date so don't modify original
				var d = new Date(dateString);
				d.setHours(0, 0, 0);
				// Set to nearest Thursday: current date + 4 - current day number
				// Make Sunday's day number 7
				d.setDate(d.getDate() + 4 - (d.getDay() || 7));
				// Get first day of year
				var yearStart = new Date(d.getFullYear(), 0, 1);
				// Calculate full weeks to nearest Thursday
				var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
				// Return array of year and week number
				return weekNo;
			};

			var incrementWeek = function(week) {
				if (!angular.isDefined(weeks[week])) {
					weeks[week] = 1;
				} else {
					weeks[week] = weeks[week] + 1;
				}
			};

			angular.forEach(workItems, function(workItem) {
				if (angular.isDefined(workItem.deployed)) {
					// get which week this deploy was made in
					var week = getWeekNumber(workItem.deployed);
					incrementWeek(week);
				}
			});

			// find first week
			var firstWeek = Object.keys(weeks).reduce(function(prev, curr) {
				if (prev < curr) {
					return prev;
				} else {
					return curr;
				}
			});

			// find last week
			var lastWeek = Object.keys(weeks).reduce(function(prev, curr) {
				if (prev > curr) {
					return prev;
				} else {
					return curr;
				}
			});

			// padding inbetween
			for (var i = firstWeek; i <= lastWeek; i++) {
				if (!angular.isDefined(weeks[i])) {
					weeks[i] = 0;
				}
			}

			// make array out of object
			angular.forEach(weeks, function(noDeploys, week) {
				weeksArray.push([week, noDeploys]);
			});

			// return weeksArray;
			return [{
				key: 'deploysPerWeek',
				values: weeksArray
				// values: [[1,2], [2,3], [3,1]]
			}];
		};

		service.getAverageLeadTime = function(workItems) {
			var leadTimes = [];

			workItems = workItems.sort(function(a, b) {
				// Turn your strings into dates, and then subtract them
				// to get a value that is either negative, positive, or zero.
				return new Date(a.start) - new Date(b.start);
			});

			console.log(workItems);

			var getLeadTimeInDays = function(workItem) {
				return Math.floor((Date.parse(workItem.done) - Date.parse(workItem.start)) / 86400000);
			};

			angular.forEach(workItems, function(workItem, index) {
				leadTimes.push([index, getLeadTimeInDays(workItem)]);
			});

			var simpleMovingAverager = function(period) {
				var nums = [];
				return function(num) {

					var sum = 0;
					var i;
					var n = period;
					nums.push(num);
					if (nums.length > period) {
						nums.splice(0, 1); // remove the first element of the array
					}
					for (i in nums) {
						if (nums.hasOwnProperty(i)) {
							sum += nums[i];
						}
					}
					if (nums.length < period) {
						n = nums.length;
					}
					return (sum / n);
				};
			};
			var sma3 = simpleMovingAverager(3);
			var leadTimesAverages = [];
			angular.forEach(leadTimes, function(leadTime) {
				leadTimesAverages.push([leadTime[0], sma3(leadTime[1])]);
			});


			return [{
				key: 'leadTimes',
				values: leadTimesAverages
			}];
		};

		return service;
	}
]);