'use strict';

angular.module('app.leadTime', ['ui.router'])

.config(['$stateProvider', '$locationProvider',
    function($stateProvider, $locationProvider) {
        var modulePath = '/components/leadtime/';

        $stateProvider
            .state('leadTime', {
                url: '/leadTime',
                templateUrl: modulePath + 'leadtime.html',
                controller: 'LeadTimeCtrl'
            });

        $locationProvider.html5Mode(true);
    }
]);