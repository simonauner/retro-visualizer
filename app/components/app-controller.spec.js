﻿/// <reference path="_references.js" />
/// <reference path="controllers.js" />

'use strict';

describe('Controllers: NavigationCtrl', function() {
    var $scope, ctrl;

    beforeEach(angular.mock.module('app.controllers'));

    beforeEach(angular.mock.inject(function($rootScope, $controller, $location) {
        $scope = $rootScope.$new();
        $location.path('/register');
        ctrl = $controller('NavigationCtrl', {
            $scope: $scope,
            $location: $location
        });
    }));

    it('should say active when viewLocation equals $location.path()', function() {
        expect($scope.isActive('/register')).toBeTruthy();
    });
    it('should say not active when viewLocation differs from $location.path()', function() {
        expect($scope.isActive('/home')).toBeFalsy();
    });
});