﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers', [])

// Path: /
.controller('HomeCtrl', ['$scope', '$state',
    function($scope, $state) {
        $scope.$root.title = 'Barometer';
        $state.go('retrospective');
    }
])

// Path: /error/404
.controller('Error404Ctrl', ['$scope',
    function($scope) {
        $scope.$root.title = 'Error 404: Page Not Found';
    }
])

.controller('NavigationCtrl', ['$scope', '$location',
    function($scope, $location) {
        $scope.isActive = function(viewLocation) {
            return viewLocation === $location.path();
        };
    }
]);