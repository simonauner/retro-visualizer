/* jshint camelcase: false */
/* global require, __dirname */
'use strict';
var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var karma = require('karma').server;

gulp.task('jshint', function() {
	gulp.src(['app/components/**/*.js', '!**/*.spec.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(jshint.reporter('fail'));
});

gulp.task('less', function() {
	return gulp.src('app/content/app.less')
		.pipe(less())
		.pipe(gulp.dest('app/content/'))
		.pipe(reload({
			stream: true
		}));
});

gulp.task('test', function(done) {
	karma.start({
		configFile: __dirname + '/karma.conf.js',
		singleRun: true
	}, done);
});

gulp.task('browsersync', ['jshint', 'less'], function() {
	browserSync({
		server: {
			baseDir: 'app'
		}
	});
});

gulp.task('default', ['less', 'browsersync'], function() {
	gulp.watch('app/content/*.less', ['less']);
});